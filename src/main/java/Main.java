import java.util.ArrayList;
import Gemstones.*;
import Gemstones.Jewelries.FirstOrderStones.*;

import static Gemstones.Gemstone.Transparency.*;

public class Main {
    public static void main(String args[]){
        Menu menu = new Menu();
        ArrayList<Gemstone> gemList = new ArrayList<Gemstone>();
        gemList.add(new Alexandrite(3,500, "Alexandrite1"));
        gemList.add(new Diamond(2,600, "Diamond"));
        gemList.add(new Emerald(4,300, "Emerald"));
        gemList.add(new Ruby(1,500, "Ruby"));
        gemList.add(new Alexandrite(4,550, "Alexandrite2"));

        menu.init(gemList, Translucent);
    }
}
