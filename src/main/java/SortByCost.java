import Gemstones.Gemstone;

import java.util.Comparator;

public class SortByCost implements Comparator<Gemstone> {
        // Used for sorting in ascending order of
        // cost
        public int compare(Gemstone a, Gemstone b)
        {
            return (int)-a.getCost() + (int)b.getCost();
        }
    }
