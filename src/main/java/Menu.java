import Gemstones.Gemstone;

import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.ArrayList;

public class Menu {
    public void init(ArrayList<Gemstone> gemList, Gemstone.Transparency transparency){
        int choice;
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println("\nPress 1 to show all gems");
            System.out.println("Press 2 to show summary of carats and cost");
            System.out.println("Press 3 to sort gems by price");
            System.out.println("Press 4 to show gems with given transparency");
            System.out.println("Press 5 to quit\n");
            choice=scanner.nextInt();
            switch(choice){
                case 1:
                    printAll(gemList);
                    break;
                case 2:
                    showSum(gemList);
                    break;
                case 3:
                    sort(gemList);
                    break;
                case 4:
                    printWithGivenTransparency(gemList, transparency);
                    break;
                default:;
            }
        }while(choice!=5);
    }

    public void printAll(ArrayList<Gemstone> gemList){
        for (Gemstone gem : gemList) {
            System.out.println(gem.getName());
        }
    }

    public void showSum(ArrayList<Gemstone> gemList){
        int caratSum=0;
        float costSum = 0f;
        for (Gemstone gem : gemList) {
            caratSum += gem.getCarat();
            costSum += gem.getCost();
        }
        System.out.println("Carats summary: " + caratSum + " and cost: " + costSum);
    }

    public void sort(ArrayList<Gemstone> gemList){
        Collections.sort(gemList, new SortByCost());
    }

    public void printWithGivenTransparency(ArrayList<Gemstone> gemList, Gemstone.Transparency transparency){
        for (Gemstone gem : gemList) {
            if(gem.getTransparency()==transparency)
                System.out.println(gem.getName());
        }
    }
}
