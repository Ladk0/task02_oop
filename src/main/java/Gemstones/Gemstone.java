package Gemstones;

import java.util.Collections;
import java.util.Comparator;

public abstract class Gemstone {
    Transparency transparency;
    private int carat;
    private float cost;
    private String name;

    public Gemstone(int carat, float cost, String name) {
        this.carat = carat;
        this.cost = cost;
        this.name = name;
    }

    public int getCarat() {
        return carat;
    }

    public void setCarat(int carat) {
        this.carat = carat;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getName(){
        return name;
    }

    public Transparency getTransparency() {
        return transparency;
    }

    public void setTransparency(Transparency transparency) {
        this.transparency = transparency;
    }

    public enum Transparency {
        Transparent,
        Translucent,
        Opaque
    }
}
