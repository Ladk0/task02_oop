package Gemstones.JewelryOrnamentals.SecondOrderStones;

public class PetrifiedWood extends SecondOrderStone {
    public PetrifiedWood(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Opaque);
    }
}
