package Gemstones.JewelryOrnamentals.SecondOrderStones;

public class Amazonite extends SecondOrderStone {
    public Amazonite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
