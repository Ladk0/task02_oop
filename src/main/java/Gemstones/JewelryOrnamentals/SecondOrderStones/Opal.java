package Gemstones.JewelryOrnamentals.SecondOrderStones;

public class Opal extends SecondOrderStone {
    public Opal(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
