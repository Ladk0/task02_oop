package Gemstones.JewelryOrnamentals.SecondOrderStones;

public class Agate extends SecondOrderStone {
    public Agate(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
