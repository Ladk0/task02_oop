package Gemstones.JewelryOrnamentals.SecondOrderStones;

import Gemstones.JewelryOrnamentals.JewelryOrnamental;

public abstract class SecondOrderStone extends JewelryOrnamental {
    public SecondOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
