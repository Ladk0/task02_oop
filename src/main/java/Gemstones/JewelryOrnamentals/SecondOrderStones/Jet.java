package Gemstones.JewelryOrnamentals.SecondOrderStones;

public class Jet extends SecondOrderStone {
    public Jet(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
