package Gemstones.JewelryOrnamentals;

import Gemstones.Gemstone;

public abstract class JewelryOrnamental extends Gemstone {
    public JewelryOrnamental(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
