package Gemstones.JewelryOrnamentals.FirstOrderStones;

public class Nephrite extends FirstOrderStone {
    public Nephrite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
