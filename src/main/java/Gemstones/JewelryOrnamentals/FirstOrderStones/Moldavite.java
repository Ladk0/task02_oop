package Gemstones.JewelryOrnamentals.FirstOrderStones;

public class Moldavite extends FirstOrderStone {
    public Moldavite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
