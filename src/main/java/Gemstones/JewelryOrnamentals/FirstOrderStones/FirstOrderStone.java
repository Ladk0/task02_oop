package Gemstones.JewelryOrnamentals.FirstOrderStones;

import Gemstones.JewelryOrnamentals.JewelryOrnamental;

public abstract class FirstOrderStone extends JewelryOrnamental {
    public FirstOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
