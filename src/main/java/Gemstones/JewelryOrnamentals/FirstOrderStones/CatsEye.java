package Gemstones.JewelryOrnamentals.FirstOrderStones;

public class CatsEye extends FirstOrderStone {
    public CatsEye(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
