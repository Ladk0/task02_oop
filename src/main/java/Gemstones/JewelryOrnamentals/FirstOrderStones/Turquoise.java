package Gemstones.JewelryOrnamentals.FirstOrderStones;

public class Turquoise extends FirstOrderStone {
    public Turquoise(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
