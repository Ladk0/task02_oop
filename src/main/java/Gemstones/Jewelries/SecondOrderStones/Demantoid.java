package Gemstones.Jewelries.SecondOrderStones;

public class Demantoid extends SecondOrderStone {
    public Demantoid(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
