package Gemstones.Jewelries.SecondOrderStones;

import Gemstones.Jewelries.Jewelry;

public abstract class SecondOrderStone extends Jewelry {
    public SecondOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
