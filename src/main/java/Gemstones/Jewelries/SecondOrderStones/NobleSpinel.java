package Gemstones.Jewelries.SecondOrderStones;

public class NobleSpinel extends SecondOrderStone {
    public NobleSpinel(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
