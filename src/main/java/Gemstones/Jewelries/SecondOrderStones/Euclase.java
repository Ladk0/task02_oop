package Gemstones.Jewelries.SecondOrderStones;

public class Euclase extends SecondOrderStone {
    public Euclase(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
