package Gemstones.Jewelries.SecondOrderStones;

public class YellowSapphire extends SecondOrderStone {
    public YellowSapphire(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
