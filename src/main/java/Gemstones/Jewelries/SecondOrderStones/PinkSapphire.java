package Gemstones.Jewelries.SecondOrderStones;

public class PinkSapphire extends SecondOrderStone {
    public PinkSapphire(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
