package Gemstones.Jewelries.FourthOrderStones;

public class Ametist extends FourthOrderStone  {
    public Ametist(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
