package Gemstones.Jewelries.FourthOrderStones;

public class Danburite extends FourthOrderStone  {
    public Danburite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
