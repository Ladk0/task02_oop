package Gemstones.Jewelries.FourthOrderStones;

public class Almandine extends FourthOrderStone {
    public Almandine(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
