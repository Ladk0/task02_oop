package Gemstones.Jewelries.FourthOrderStones;

public class Dioptase extends FourthOrderStone  {
    public Dioptase(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
