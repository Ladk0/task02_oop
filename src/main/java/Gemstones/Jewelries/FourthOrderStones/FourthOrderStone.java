package Gemstones.Jewelries.FourthOrderStones;

import Gemstones.Jewelries.Jewelry;

public abstract class FourthOrderStone extends Jewelry {
    public FourthOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
