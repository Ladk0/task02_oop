package Gemstones.Jewelries.FourthOrderStones;

public class Adular extends FourthOrderStone {
    public Adular(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
