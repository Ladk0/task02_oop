package Gemstones.Jewelries.ThirdOrderStones;

public class Zavorite extends ThirdOrderStone {
    public Zavorite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
