package Gemstones.Jewelries.ThirdOrderStones;

import Gemstones.Jewelries.Jewelry;

public abstract class ThirdOrderStone extends Jewelry {
    public ThirdOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
