package Gemstones.Jewelries.ThirdOrderStones;

public class Tanzanite extends ThirdOrderStone {
    public Tanzanite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
