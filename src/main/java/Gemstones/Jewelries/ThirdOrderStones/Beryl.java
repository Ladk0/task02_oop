package Gemstones.Jewelries.ThirdOrderStones;

public class Beryl extends ThirdOrderStone {
    public Beryl(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
