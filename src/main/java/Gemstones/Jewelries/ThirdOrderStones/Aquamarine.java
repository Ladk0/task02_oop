package Gemstones.Jewelries.ThirdOrderStones;

public class Aquamarine extends ThirdOrderStone {
    public Aquamarine(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
