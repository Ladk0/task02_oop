package Gemstones.Jewelries.ThirdOrderStones;

public class Tourmaline extends ThirdOrderStone {
    public Tourmaline(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
