package Gemstones.Jewelries;

import Gemstones.Gemstone;

public abstract class Jewelry extends Gemstone {
    public Jewelry(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
