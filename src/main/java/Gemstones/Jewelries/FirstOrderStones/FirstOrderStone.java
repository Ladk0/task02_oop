package Gemstones.Jewelries.FirstOrderStones;

import Gemstones.Jewelries.Jewelry;

public abstract class FirstOrderStone extends Jewelry {
    public FirstOrderStone(int carat, float cost, String name){
        super(carat, cost, name);
    }
}
