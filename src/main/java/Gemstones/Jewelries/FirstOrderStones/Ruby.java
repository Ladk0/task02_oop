package Gemstones.Jewelries.FirstOrderStones;

public class Ruby extends FirstOrderStone {
    public Ruby(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
