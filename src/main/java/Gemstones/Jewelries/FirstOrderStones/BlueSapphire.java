package Gemstones.Jewelries.FirstOrderStones;

public class BlueSapphire extends FirstOrderStone {
    public BlueSapphire(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
