package Gemstones.Jewelries.FirstOrderStones;

public class Diamond extends FirstOrderStone {
    public Diamond(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Transparent);
    }
}
