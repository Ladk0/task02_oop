package Gemstones.Jewelries.FirstOrderStones;

public class Alexandrite extends FirstOrderStone {
    public Alexandrite(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
