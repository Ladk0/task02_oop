package Gemstones.Jewelries.FirstOrderStones;

public class Emerald extends FirstOrderStone {
    public Emerald(int carat, float cost, String name){
        super(carat, cost, name);
        this.setTransparency(Transparency.Translucent);
    }
}
